//
//  HomeViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 7.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import VersionTrackerSwift
import Alamofire
import SwiftOverlays
import CoreData
import MapKit

class HomeViewController: UIViewController, MKMapViewDelegate {

    var notificationView: NotificationView!
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showNotification(_:)), name:NSNotification.Name(rawValue: "showNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.closeModal), name:NSNotification.Name(rawValue: "closeModal"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.registerDevice), name:NSNotification.Name(rawValue: "registerDevice"), object: nil)

        self.mapView.delegate = self
        
//        self.registerDevice()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navBarHeight = UIApplication.shared.statusBarFrame.height+self.navigationController!.navigationBar.frame.height
        appDelegate.NAV_HEIGHT = Int(navBarHeight)
        
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
        
        if let notification = UserDefaults.standard.value(forKey: "notification") {
            self.perform(#selector(self.showNotification), with: notification, afterDelay: 1.0)
            UserDefaults.standard.removeObject(forKey: "notification")
        }
        
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.addAnnotations()
    }

    func registerDevice() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "device_token": (self.getUser()?.token)!
        ]
        
        let user = self.getUser()
        if((user?.token?.characters.count)! > 0) {
            Alamofire.request(String(appDelegate.API_URL+"registerDevice"), method: .post, parameters: parameters)
                .validate()
                .responseJSON {response in
                    if(response.result.isSuccess) {
                        let json = response.result.value! as! [String: Any]
                        user?.user_id = Int32((json["user_id"] as! NSNumber))
                        appDelegate.updateEventsAndBeacons(data: (json["events"] as? NSArray)!)
                        do {
                            try user?.managedObjectContext?.save()
                        } catch {
                            fatalError("Failure to save context in update user: \(error)")
                        }
                    } else {
                        if let data = response.data {
                            if let json = self.dataToJSON(data: data) as? [String: Any] {
                                self.notificationView.textLabelText = json["error"] as? String
                            } else {
                                self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                            }
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                        
                        self.notificationView.titleLabelText = ""
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                    }
            }
        }
    }
    
    func showNotification(_ notification: Notification) -> Void {
        
        let userinfo = notification.userInfo
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ticketViewController = storyboard.instantiateViewController(withIdentifier: "TicketDetailViewController") as! TicketDetailViewController
        ticketViewController.event_id = Int(userinfo?["event_id"] as! Int)
        ticketViewController.modalPresentationStyle = .overCurrentContext
        self.present(ticketViewController, animated: true, completion: nil)
    }
    
    func addAnnotations() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<Event>
        if #available(iOS 10.0, *) {
            request = Event.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "Event")
        }
        
        do {
            let events = try context.fetch(request)
            for event in events {
                
                let annotation = MyAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: event.event_latitude, longitude: event.event_longitude)
                annotation.event = event
                mapView.addAnnotation(annotation)
            }
            
        } catch let error {
            print("Error in get events in Event list: "+error.localizedDescription)
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationID"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            annotationView.image = UIImage(named: "PinIcon")
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let myAnnotation = view.annotation as! MyAnnotation
        let event = myAnnotation.event!
        
        mapView.deselectAnnotation(myAnnotation, animated: true)
        
        self.tabBarController?.tabBar.isHidden = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ticketViewController = storyboard.instantiateViewController(withIdentifier: "TicketDetailViewController") as! TicketDetailViewController
        ticketViewController.event_id = Int(event.event_id)
        ticketViewController.modalPresentationStyle = .overCurrentContext
        self.present(ticketViewController, animated: true, completion: nil)
    }
    
    func closeModal() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

