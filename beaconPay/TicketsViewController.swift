//
//  TicketsViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 29.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PopupDialog
import EasyNotificationBadge

class TicketsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var loader: NVActivityIndicatorView!
    var notificationView: NotificationView!
    var tickets: [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        
        NotificationCenter.default.addObserver(self, selector: #selector(TicketsViewController.loadTickets), name:NSNotification.Name(rawValue: "afterLogin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TicketsViewController.loadTickets), name:NSNotification.Name(rawValue: "preloadTickets"), object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = UIColor(hex: "2B2E33")
        self.view.addSubview(loader)
        
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
        
        if (self.getUser()?.user_logged)! {
            self.loadTickets()
        } else {
            self.tabBarController?.tabBar.isHidden = true
            self.performSegue(withIdentifier: "showLogin", sender: nil)
        }
    }
    
    func loadTickets() {
        
        self.loader.startAnimating()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "user_id": (self.getUser()?.user_id)!
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"tickets"), method: .get, parameters: parameters)
            .validate()
            .responseJSON {response in
                if(response.result.isSuccess) {
                    let json = response.result.value! as! [String: Any]
                    self.tickets = (json["tickets"] as! NSArray) as Array
                    self.tableView.reloadData()
                } else {
                    if let data = response.data {
                        if let json = self.dataToJSON(data: data) as? [String: Any] {
                            self.notificationView.textLabelText = json["error"] as? String
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    } else {
                        self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                    }
                    
                    self.notificationView.titleLabelText = ""
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                }
                self.loader.stopAnimating()
        }
        
    }
    
    // Table view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TicketsViewCell
        let ticket = tickets[indexPath.row] as! NSDictionary
        cell.event_title.text = ticket["event_title"] as? String
        cell.option_title.text = ticket["event_option_title"] as? String
        cell.checkboxBackgroundView.badge(text: ticket["quantity"] as? String)
        if ((ticket["order_item_active"] as? String)! == "1") {
            cell.okIcon.isHidden = true
        } else {
            cell.okIcon.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ticket = tickets[indexPath.row] as! NSDictionary
        DispatchQueue.main.async {
            if ((ticket["order_item_active"] as? String)! == "1") {
                self.tabBarController?.tabBar.isHidden = true
                self.performSegue(withIdentifier: "showTicket", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showTicket") {
            let nextScene =  segue.destination as! TicketModalViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let ticket = tickets[indexPath.row] as! NSDictionary
                nextScene.ticket = ticket
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
