//
//  RegisterViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 19.4.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class RegisterViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstnameField: FormField!
    @IBOutlet weak var lastnameField: FormField!
    @IBOutlet weak var emailField: FormField!
    @IBOutlet weak var phoneField: FormField!
    @IBOutlet weak var passField: FormField!
    @IBOutlet weak var passAgainField: FormField!
    
    var notificationView: NotificationView!
    var loader: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let keyboardTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        self.view.addGestureRecognizer(keyboardTap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = .white
        self.view.addSubview(loader)
        
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
    }

    @IBAction func submitAction(_ sender: Any) {
        
        if(self.firstnameField.text?.characters.count == 0) {
            self.firstnameField.becomeFirstResponder()
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Zadajte krstné meno"
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
            return
        }
        
        if(self.lastnameField.text?.characters.count == 0) {
            self.lastnameField.becomeFirstResponder()
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Zadajte priezvisko"
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
            return
        }
        
        if(self.emailField.text?.characters.count == 0) {
            self.emailField.becomeFirstResponder()
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Zadajte email"
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
            return
        }
        
        if(self.passField.text?.characters.count == 0) {
            self.passField.becomeFirstResponder()
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Zadajte heslo"
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
            return
        }
    
        if(self.passAgainField.text?.characters.count == 0) {
            self.passAgainField.becomeFirstResponder()
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Zadajte heslo znova"
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
            return
        }
        
        if (self.passField.text != self.passAgainField.text) {
            self.passField.becomeFirstResponder()
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Heslá sa nezhodujú"
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
            return
        }
        
        self.loader.startAnimating()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "user_firstname": self.firstnameField.text!,
            "user_lastname": self.lastnameField.text!,
            "user_mail": self.emailField.text!,
            "user_phone": self.phoneField.text!,
            "user_password": self.passField.text!,
            "device_token": (self.getUser()?.token!)! as String
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"register"), method: .post, parameters: parameters)
            .validate()
            .responseJSON {response in
                if(response.result.isSuccess) {
                    _ = self.navigationController?.popViewController(animated: true)
                } else {
                    
                    if let data = response.data {
                        if let json = self.dataToJSON(data: data) as? [String: Any] {
                            self.notificationView.textLabelText = json["error"] as? String
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    } else {
                        self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                    }
                    
                    self.notificationView.titleLabelText = ""
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                }
                self.loader.stopAnimating()
        }

        
    }
    
    func dismissKeyboard(_ gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func keyboardWillShow(notification:NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height+100
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification) {
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
