//
//  TicketsViewCell.swift
//  beaconPay
//
//  Created by Patrik Dendis on 18.4.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit

class TicketsViewCell: UITableViewCell {

    
    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet weak var option_title: UILabel!
    @IBOutlet weak var event_title: UILabel!
    @IBOutlet weak var okIcon: UIImageView!
    @IBOutlet weak var checkboxBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkboxView.layer.borderWidth = 2
        checkboxView.layer.borderColor = UIColor.lightGray.cgColor
        checkboxView.layer.cornerRadius = 10
        checkboxView.clipsToBounds = true
        checkboxView.backgroundColor = UIColor.clear
        okIcon.image = okIcon.image!.withRenderingMode(.alwaysTemplate)
        okIcon.tintColor = UIColor(red:0.09, green:0.65, blue:0.54, alpha:1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
