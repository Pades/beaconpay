//
//  Extensions.swift
//  beaconPay
//
//  Created by Patrik Dendis on 16.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit

extension Date {
    var timestamp: Int {
        return Int(self.timeIntervalSince1970)
    }
}
