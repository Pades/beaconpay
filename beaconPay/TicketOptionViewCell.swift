//
//  TicketOptionViewCell.swift
//  beaconPay
//
//  Created by Patrik Dendis on 29.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit

class TicketOptionViewCell: UITableViewCell {

    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var okIcon: UIImageView!
    @IBOutlet weak var checkBoxBackground: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {

        super.awakeFromNib()
        checkboxView.layer.borderWidth = 2
        checkboxView.layer.borderColor = UIColor(hex: "717171").cgColor
        checkboxView.layer.cornerRadius = 10
        checkboxView.clipsToBounds = true
        checkboxView.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
