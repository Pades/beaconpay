//
//  ProfileViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 29.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ProfileViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    
    var notificationView: NotificationView!
    var loader: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.loadProfile), name:NSNotification.Name(rawValue: "afterLogin"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = UIColor(hex: "2B2E33")
        self.view.addSubview(loader)
        
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
        
        if (self.getUser()?.user_logged)! {
            self.loadProfile()
        } else {
            self.tabBarController?.tabBar.isHidden = true
            self.performSegue(withIdentifier: "showLogin", sender: nil)
        }
    }
    
    func loadProfile() {
        self.loader.startAnimating()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "user_id": (self.getUser()?.user_id)!
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"user"), method: .get, parameters: parameters)
            .validate()
            .responseJSON {response in
                if(response.result.isSuccess) {
                    let json = response.result.value! as! [String: Any]
                    let userJson = json["user"] as! [String: Any]
                    self.nameLabel.text = (userJson["user_firstname"] as! String)+" "+(userJson["user_lastname"] as! String)
                    self.mailLabel.text = userJson["user_mail"] as? String
                    self.phoneLabel.text = userJson["user_phone"] as? String
                    self.coinLabel.text = userJson["user_coin"] as? String
                } else {
                    if let data = response.data {
                        if let json = self.dataToJSON(data: data) as? [String: Any] {
                            self.notificationView.textLabelText = json["error"] as? String
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    } else {
                        self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                    }
                    
                    self.notificationView.titleLabelText = ""
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                }
                self.loader.stopAnimating()
        }
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        let user = self.getUser()
        do {
            user?.user_logged = false
            try user?.managedObjectContext?.save()
        } catch let error {
            print(error.localizedDescription)
        }
        
        tabBarController!.selectedIndex = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
