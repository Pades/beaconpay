//
//  MyAnnotation.swift
//  beaconPay
//
//  Created by Patrik Dendis on 30.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import MapKit

class MyAnnotation: MKPointAnnotation {
    var event: Event! = nil
}
