//
//  UIViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 23.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController {

    func getUser() -> User? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<User>
        if #available(iOS 10.0, *) {
            request = User.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "User")
        }
        do {
            let users = try context.fetch(request)
            return users.first!
        } catch let error {
            print("Error in getUser: "+error.localizedDescription)
        }
        return nil
    }
    
    func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
            
}
