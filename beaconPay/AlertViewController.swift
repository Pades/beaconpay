//
//  AlertViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 24.10.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit

@objc protocol AlertDelegate {
    @objc optional func alertLeftButtonAction(sender: AnyObject)
    @objc optional func alertCenterButtonAction(sender: AnyObject)
    @objc optional func alertRightButtonAction(sender: AnyObject)
}


class AlertViewController: UIViewController {

    var delegate: AlertDelegate?
    var mainVC: UIViewController!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    var name:String = ""
    var text:String = ""
    var nameLeftButton:String = ""
    var nameCenterButton:String = ""
    var nameRightButton:String = ""
    var leftButtonHidden:Bool = true
    var centerButtonHidden:Bool = true
    var rightButtonHidden:Bool = true
    var buttonTag = 0
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftButton.isHidden = leftButtonHidden
        self.centerButton.isHidden = centerButtonHidden
        self.rightButton.isHidden = rightButtonHidden
        self.leftButton.setTitle(nameLeftButton, for: .normal)
        self.centerButton.setTitle(nameCenterButton, for: .normal)
        self.rightButton.setTitle(nameRightButton, for: .normal)
        self.nameLabel.text = name
        self.textLabel.text = text
        self.centerButton.tag = buttonTag
    }
    
    @IBAction func alertLeftButtonAction(_ sender: AnyObject) {
        delegate?.alertLeftButtonAction!(sender: sender)
    }
    
    @IBAction func alertCenterButtonAction(_ sender: AnyObject) {
        delegate?.alertCenterButtonAction!(sender: sender)
    }

    @IBAction func alertRightButtonAction(_ sender: AnyObject) {
        delegate?.alertRightButtonAction!(sender: sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
