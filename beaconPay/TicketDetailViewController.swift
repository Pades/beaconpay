//
//  TicketDetailViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 29.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PopupDialog
import EasyNotificationBadge

class TicketDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, QuantityDelegate, AlertDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var optionsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var showMapButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
    var quantityAlert: QuantityViewController!
    var alertVC: AlertViewController!
    var event_id: Int = 0
    var options: NSArray = []
    var event: Event!
    var loader: NVActivityIndicatorView!
    var notificationView: NotificationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.event = appDelegate.getEventById(id: event_id)
        self.titleLabel.text = event.event_title
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = .white
        self.view.addSubview(loader)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsMultipleSelection = true
        
        let attrs = [NSUnderlineStyleAttributeName : 1]
        self.showMapButton.setAttributedTitle(NSMutableAttributedString(string:(self.showMapButton.titleLabel?.text)!, attributes:attrs), for: .normal)
        self.infoButton.setAttributedTitle(NSMutableAttributedString(string:(self.infoButton.titleLabel?.text)!, attributes:attrs), for: .normal)//
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadEvent()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
    }
    
    func loadEvent() {
        
        self.loader.startAnimating()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "event_id": event_id
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"eventDetail"), method: .get, parameters: parameters)
            .validate()
            .responseJSON {response in
                if(response.result.isSuccess) {
                    let json = response.result.value! as! [String: Any]
                    
                    self.options = json["options"] as! NSArray
                    
                    self.view.layoutIfNeeded()
                    UIView.animate(withDuration: 0.1, animations: {
                        self.optionsViewHeight.constant = CGFloat(70*self.options.count)
                        self.contentHeight.constant = CGFloat(90+(70*self.options.count)+20+45+15+35+5+35) // Header height (100) + 70*count options + margin top pay button (20) + pay button height (45) + two buttons with height 35 and top margin 5
                        self.view.layoutIfNeeded()
                    })
                    self.tableView.reloadData()
                    
                } else {
                    if let data = response.data {
                        if let json = self.dataToJSON(data: data) as? [String: Any] {
                            self.notificationView.textLabelText = json["error"] as? String
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    } else {
                        self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                    }
                    
                    self.notificationView.titleLabelText = ""
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                }
                self.loader.stopAnimating()
            }
    
    }
    
    @IBAction func payAction(_ sender: Any) {
        
        var fullPrice = 0.0
        self.tableView.indexPathsForSelectedRows?.forEach {indexPath in
            let currentCell = tableView.cellForRow(at: indexPath) as! TicketOptionViewCell
            let quantity = Double(currentCell.checkBoxBackground.tag)
            let price = Double(currentCell.priceLabel.text!)
            
            fullPrice += quantity*price!
        }
        
        var text:String = ""
        if(fullPrice > 0) {
            text = "Chcete skutočne urobiť objednávku vo výške "+String(fullPrice)+"€ ?"
        } else {
            text = "Váš košík je prázdny."
        }
        
        alertVC = AlertViewController(nibName: "AlertViewController", bundle: nil)
        alertVC.leftButtonHidden = false
        alertVC.nameLeftButton = "Zrušiť"
        alertVC.rightButtonHidden = false
        alertVC.nameRightButton = "Potvrdiť"
        alertVC.delegate = self
        alertVC.mainVC = self
        alertVC.name = "Objednávka"
        alertVC.text = text
        let popup = PopupDialog(viewController: alertVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
        DispatchQueue.main.async {
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    // TableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TicketOptionViewCell
        let obj = options[indexPath.row] as! [String: Any]
        
        let title = NSMutableAttributedString(string:(obj["event_option_title"] as? String!)!+" - ")
        let priceAttributes = [NSForegroundColorAttributeName: UIColor(hex: "c75757")]
        let price = NSMutableAttributedString(string: (obj["event_option_price"] as? String!)!+"€", attributes: priceAttributes)
        title.append(price)
        
        cell.priceLabel.text = (obj["event_option_price"] as? String!)!
        cell.titleLabel.attributedText = title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! TicketOptionViewCell
        currentCell.okIcon.isHidden = true
        currentCell.checkBoxBackground.badge(text: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath) as! TicketOptionViewCell
        let obj = options[indexPath.row] as! [String: Any]
        quantityAlert = QuantityViewController(nibName: "QuantityViewController", bundle: nil)
        quantityAlert.cell = currentCell
        quantityAlert.leftButtonHidden = false
        quantityAlert.rightButtonHidden = false
        quantityAlert.nameLeftButton = "Zrušiť"
        quantityAlert.nameRightButton = "Potvrdiť"
        quantityAlert.delegate = self
        quantityAlert.mainVC = self
        quantityAlert.name = "Množstvo"
        quantityAlert.text = "1"
        quantityAlert.option = obj
        let popup = PopupDialog(viewController: quantityAlert, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
        DispatchQueue.main.async {
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    // Quantity alert delegate methods
    func leftButtonAction(sender: AnyObject) {
        quantityAlert.dismiss(animated: true, completion: {
            self.quantityAlert = nil
        })
    }
    
    func rightButtonAction(sender: AnyObject, obj: AnyObject) {
        quantityAlert.dismiss(animated: true, completion: {
            self.quantityAlert = nil
        })
    }
    
    // Alert delegate methods
    func alertLeftButtonAction(sender: AnyObject) {
        alertVC.dismiss(animated: true, completion: {
            self.alertVC = nil
        })
    }
    
    func alertRightButtonAction(sender: AnyObject) {
        alertVC.dismiss(animated: true, completion: {
            self.alertVC = nil
            self.submitPayAction()
        })
    }
    
    func submitPayAction() {
        
        self.loader.startAnimating()
        var items = [Any]()
        self.tableView.indexPathsForSelectedRows?.forEach {indexPath in
            let currentCell = tableView.cellForRow(at: indexPath) as! TicketOptionViewCell
            let item = options[indexPath.row] as! [String: Any]
            let quantity = Int(currentCell.checkBoxBackground.tag)
            let option = NSMutableDictionary()
            option.setValue(item["event_option_id"]!, forKey : "event_option_id")
            option.setValue(String(format:"%d", quantity), forKey : "quantity")
            items.append(option)
        }
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "user_id": (self.getUser()?.user_id)!,
            "options": items
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"orderTicket"), method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON {response in
                
                if(response.result.isSuccess) {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "closeModal"), object: nil, userInfo: nil)
                    self.dismiss(animated: true, completion: {
                        self.notificationView.titleLabelText = ""
                        self.notificationView.textLabelText = "Lístok je úspešne zakúpený."
                        self.notificationView.backView.backgroundColor = UIColor(hex: "18A689")
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                    })
                } else {
                    
                    if let data = response.data {
                        if let json = self.dataToJSON(data: data) as? [String: Any] {
                            self.notificationView.textLabelText = json["error"] as? String
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    } else {
                        self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                    }
                    
                    self.notificationView.titleLabelText = ""
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                }
                self.loader.stopAnimating()
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "closeModal"), object: nil, userInfo: nil)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
