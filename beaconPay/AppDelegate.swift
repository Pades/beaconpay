//
//  AppDelegate.swift
//  beaconPay
//
//  Created by Patrik Dendis on 7.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import CoreData
import VersionTrackerSwift

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, ESTBeaconManagerDelegate {

    let API_URL = "http://beaconpay.dsoft.bid/api/"
    var window: UIWindow?
    let beaconManager = ESTBeaconManager()
    var NAV_HEIGHT = 50
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        VersionTracker.track()
        
        if(VersionTracker.isFirstLaunchEver()) {
            let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: managedObjectContext) as! User
            user.token = ""
            do {
                try managedObjectContext.save()
            } catch {
                fatalError("Failure to save context new user: \(error)")
            }
        }
        
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        
        UITabBar.appearance().tintColor = UIColor.white        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.shadowImage = UIImage()
        navigationBarAppearace.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBarAppearace.titleTextAttributes = [
            NSFontAttributeName: (UIFont(name: "Avenir-Book", size: 20))!,
            NSForegroundColorAttributeName : UIColor.white
        ]
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
        if let notification = launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as? [String: AnyObject] {
            UserDefaults.standard.set(notification, forKey: "notification")
        }
        
        print(self.beaconManager.monitoredRegions)
        
        return true
    }
    
    // Register remote notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print(token)
        
        let context: NSManagedObjectContext = managedObjectContext
        let request: NSFetchRequest<User>
        if #available(iOS 10.0, *) {
            request = User.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "User")
        }
        do {
            let users = try context.fetch(request)
            let user = users.first
            if(user?.token != token) {
                user?.token = token
                try user?.managedObjectContext?.save()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "registerDevice"), object: nil, userInfo: nil)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    // Remote notifications error response
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        let token = (UIDevice.current.identifierForVendor?.uuidString)! as String
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<User>
        if #available(iOS 10.0, *) {
            request = User.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "User")
        }
        do {
            let users = try context.fetch(request)
            let user = users.first
            user?.token = token
            try user?.managedObjectContext?.save()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    // Recieve silent remote notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let aps = userInfo["aps"] as! [String: AnyObject]
        if (aps["content-available"] as? NSString)?.integerValue == 1 {
            if let data = (userInfo["data"] as? NSArray) {
                self.updateEventsAndBeacons(data: data)
            }
        }
        
    }
    
    // Update events and their beacons
    func updateEventsAndBeacons(data :NSArray) {
        print("Updated events and beacons")
        self.deleteAllBeacons()
        let managedContext = managedObjectContext
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        for itemEvent in data {
            let event = itemEvent as! [String: AnyObject]
            let newEvent = NSEntityDescription.insertNewObject(forEntityName: "Event", into: managedContext) as! Event
            newEvent.event_title = event["event_title"] as? String
            newEvent.event_id = Int32(event["event_id"] as! Int)
            newEvent.event_latitude = event["event_latitude"] as! Double
            newEvent.event_longitude = event["event_longitude"] as! Double
            newEvent.event_startdate = dateFormatter.date(from: (event["event_startdate"] as! String)) as NSDate?
            if let endDate = (event["event_enddate"] as? String) {
                newEvent.event_enddate = dateFormatter.date(from: endDate) as NSDate?
            } else {
                newEvent.event_enddate = nil
            }
            var beacons = Set<Beacon>()
            
            for itemBeacon in (event["beacons"] as? NSArray)! {
                let beacon = itemBeacon as! [String: AnyObject]
                let newBeacon = NSEntityDescription.insertNewObject(forEntityName: "Beacon", into: managedContext) as! Beacon
                
                newBeacon.beacon_uuid = beacon["beacon_uuid"] as! String?
                newBeacon.beacon_major = Int32((beacon["beacon_major"] as! String))!
                newBeacon.beacon_minor = Int32((beacon["beacon_minor"] as! String))!
                newBeacon.beacon_id = Int32((beacon["beacon_id"] as! String))!
                
                beacons.insert(newBeacon)
               
                let identifier = ((beacon["beacon_id"] as! String)+"_"+String(describing: event["event_id"]!))
                self.beaconManager.startMonitoring(for: CLBeaconRegion(
                    proximityUUID: UUID(uuidString: newBeacon.beacon_uuid!)!,
                    major: CLBeaconMajorValue(newBeacon.beacon_major), minor: CLBeaconMinorValue(newBeacon.beacon_minor), identifier: identifier))
            
            }
            
            newEvent.setValue(beacons, forKey: "beacons")
            
            do {
                try managedContext.save()
            } catch {
                fatalError("Failure to save context new station: \(error)")
            }
        }
    }
    
    // Stop monitoring all beacons and delete local database
    func deleteAllBeacons() {
        
        self.beaconManager.stopMonitoringForAllRegions()
        let managedContext = managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Beacon")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
        let deleteRequest2 = NSBatchDeleteRequest(fetchRequest: fetchRequest2)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.execute(deleteRequest2)
            try managedContext.save()
        }
        catch let error as NSError {
            print("Error in deleteAllStations: "+error.localizedDescription)
        }
    }
    
    // Recieve local notification
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "showNotification"), object: nil, userInfo: notification.userInfo)
    }
    
    // iBeacon recieve enter
    func beaconManager(_ manager: Any, didEnter region: CLBeaconRegion) {
        print("Teraz")
        let identifierArray = region.identifier.components(separatedBy: "_")
        let event_id = Int(identifierArray[1])
        if let event = self.getEventById(id: event_id!) {
            
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.alertBody =
                "Zakúpte si lístok na " + event.event_title! + "!"
            notification.userInfo = ["event_id": event.event_id]
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
//        if let last_sent = UserDefaults.standard.value(forKey: "last_sent") {
//            let now = Date().timestamp
//            if(now - (last_sent as! Int) > 600) { // 600 = 10 minutes
//                UIApplication.shared.presentLocalNotificationNow(notification)
//                UserDefaults.standard.set(Date().timestamp, forKey: "last_sent")
//            }
//        } else {
//            UIApplication.shared.presentLocalNotificationNow(notification)
//            UserDefaults.standard.set(Date().timestamp, forKey: "last_sent")
//        }
    }
    
    // Get event from local database by event_id
    func getEventById(id : Int) -> Event? {
        
        let context: NSManagedObjectContext = managedObjectContext
        let request: NSFetchRequest<Event>
        if #available(iOS 10.0, *) {
            request = Event.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "Event")
        }
        request.predicate = NSPredicate(format: "event_id == %d", id)
        do {
            let events = try context.fetch(request)
            return events.first!
        } catch let error {
            print("Error in getEvent by ID: "+error.localizedDescription)
        }
        return nil
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "sk.itway.Zlty_odchytavac" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "beaconPay", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }


}

