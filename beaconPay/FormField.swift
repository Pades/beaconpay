//
//  FormField.swift
//  beaconPay
//
//  Created by Patrik Dendis on 14.4.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit

class FormField: UITextField {

    var padding: CGFloat = 10

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 5.0
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }

}
