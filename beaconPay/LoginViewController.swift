//
//  LoginViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 14.4.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: FormField!
    @IBOutlet weak var passField: FormField!
    @IBOutlet weak var registerButton: UIButton!
    
    var notificationView: NotificationView!
    var loader: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = .white
        self.view.addSubview(loader)
        
        let attrs = [NSUnderlineStyleAttributeName : 1]
        self.registerButton.setAttributedTitle(NSMutableAttributedString(string:(self.registerButton.titleLabel?.text)!, attributes:attrs), for: .normal)
        
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        if((self.emailField.text?.characters.count)! > 0 && (self.passField.text?.characters.count)! > 0) {
            
            self.loader.startAnimating()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let parameters: Parameters = [
                "user_mail": self.emailField.text!,
                "user_password": self.passField.text!,
                "device_token": (self.getUser()?.token!)! as String
            ]
            
            Alamofire.request(String(appDelegate.API_URL+"login"), method: .post, parameters: parameters)
                .validate()
                .responseJSON {response in

                    if(response.result.isSuccess) {
                        let json = response.result.value! as! [String: Any]
                        let userJson = json["user"] as! [String: Any]
                        let user = self.getUser()
                        do {
                            user?.user_id = Int32(userJson["user_id"] as! String)!
                            user?.user_logged = true
                            try user?.managedObjectContext?.save()
                        } catch let error {
                            print(error.localizedDescription)
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "closeModal"), object: nil, userInfo: nil)
                        self.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "afterLogin"), object: nil, userInfo: nil)
                        })
                        
                    } else {
                        
                        if let data = response.data {
                            if let json = self.dataToJSON(data: data) as? [String: Any] {
                                self.notificationView.textLabelText = json["error"] as? String
                            } else {
                                self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                            }
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    
                        self.notificationView.titleLabelText = ""
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                    }
                    self.loader.stopAnimating()
            }
            
        } else {
            self.notificationView.titleLabelText = ""
            self.notificationView.textLabelText = "Nezadali ste všetky potrebné údaje."
            UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
        }
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "closeModal"), object: nil, userInfo: nil)
        dismiss(animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
