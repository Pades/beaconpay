//
//  TicketModalViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 19.4.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftQRCode
import NVActivityIndicatorView

class TicketModalViewController: UIViewController {

    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var codeLabel: UILabel!
    
    var notificationView: NotificationView!
    var loader: NVActivityIndicatorView!
    var ticket: NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.codeLabel.text = ticket["order_item_id"] as? String
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = UIColor(hex: "2B2E33")
        self.view.addSubview(loader)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        qrImageView.image = QRCode.generateImage((ticket["order_item_id"] as? String)!, avatarImage: UIImage(named: "avatar"))
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
    }

    @IBAction func activeAction(_ sender: Any) {
        self.loader.startAnimating()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let parameters: Parameters = [
            "order_item_id": (ticket["order_item_id"] as? String)!
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"useTicket"), method: .post, parameters: parameters)
            .validate()
            .responseJSON {response in
                print(response.request.debugDescription)
                if((response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300) {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "closeModal"), object: nil, userInfo: nil)
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "preloadTickets"), object: nil, userInfo: nil)
                    })
                } else {
                    if let data = response.data {
                        if let json = self.dataToJSON(data: data) as? [String: Any] {
                            self.notificationView.textLabelText = json["error"] as? String
                        } else {
                            self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                        }
                    } else {
                        self.notificationView.textLabelText = "Serverová chyba. Na chybe pracujeme."
                    }
                    
                    self.notificationView.titleLabelText = ""
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 2.0)
                }
                self.loader.stopAnimating()
        }

    }
    
    @IBAction func closeAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "closeModal"), object: nil, userInfo: nil)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
