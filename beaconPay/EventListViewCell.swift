//
//  EventListViewCell.swift
//  beaconPay
//
//  Created by Patrik Dendis on 30.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit

class EventListViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
