//
//  QuantityViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 29.03.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit

@objc protocol QuantityDelegate {
    @objc optional func leftButtonAction(sender: AnyObject)
    @objc optional func centerButtonAction(sender: AnyObject)
    @objc optional func rightButtonAction(sender: AnyObject, obj: AnyObject)
}

class QuantityViewController: UIViewController {

    var delegate: QuantityDelegate?
    var mainVC: UIViewController!
    var cell: TicketOptionViewCell!
    var name:String = ""
    var text:String = ""
    var nameLeftButton:String = ""
    var nameCenterButton:String = ""
    var nameRightButton:String = ""
    var leftButtonHidden:Bool = true
    var centerButtonHidden:Bool = true
    var rightButtonHidden:Bool = true
    var buttonTag = 0
    var option: [String: Any] = [:]
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var quantityStepper: UIStepper!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftButton.isHidden = leftButtonHidden
        self.centerButton.isHidden = centerButtonHidden
        self.rightButton.isHidden = rightButtonHidden
        self.leftButton.setTitle(nameLeftButton, for: .normal)
        self.centerButton.setTitle(nameCenterButton, for: .normal)
        self.rightButton.setTitle(nameRightButton, for: .normal)
        self.nameLabel.text = name
        self.textLabel.text = text
        self.centerButton.tag = buttonTag
        
        self.quantityStepper.minimumValue = Double(option["event_option_min_quantity"] as! String)!
        self.quantityStepper.value = Double(option["event_option_min_quantity"] as! String)!
        if(Int(option["event_option_max_quantity"] as! String)! > 0) {
            self.quantityStepper.maximumValue = Double(option["event_option_max_quantity"] as! String)!
        }
    }
    
    @IBAction func quantityAction(_ sender: Any) {
        self.textLabel.text = Int((sender as! UIStepper).value).description
    }

    @IBAction func leftButtonAction(_ sender: AnyObject) {
        cell.checkBoxBackground.badge(text: nil)
        cell.checkBoxBackground.tag = 0
        cell.okIcon.isHidden = true
        delegate?.leftButtonAction!(sender: sender)
    }
    
    @IBAction func centerButtonAction(_ sender: AnyObject) {
        delegate?.centerButtonAction!(sender: sender)
    }

    @IBAction func rightButtonAction(_ sender: AnyObject) {
        if(Int(self.textLabel.text!)! > 0) {
            cell.checkBoxBackground.badge(text: self.textLabel.text)
            cell.checkBoxBackground.tag = Int(self.textLabel.text!)!
            cell.okIcon.isHidden = false
        }
        delegate?.rightButtonAction!(sender: sender, obj: cell)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
