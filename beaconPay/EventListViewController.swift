//
//  EventListViewController.swift
//  beaconPay
//
//  Created by Patrik Dendis on 29.3.17.
//  Copyright © 2017 ITway.sk. All rights reserved.
//

import UIKit
import CoreData

class EventListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var events: [Event] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadEvents()
    }
    
    func loadEvents() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<Event>
        if #available(iOS 10.0, *) {
            request = Event.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "Event")
        }
        
        do {
            events = try context.fetch(request)
            self.tableView.reloadData()
            
        } catch let error {
            print("Error in get events in Event list: "+error.localizedDescription)
        }
    }
    
    // Table view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EventListViewCell
        let event = events[indexPath.row]
        cell.titleLabel.text = event.event_title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let event = events[indexPath.row]
        
        self.tabBarController?.tabBar.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ticketViewController = storyboard.instantiateViewController(withIdentifier: "TicketDetailViewController") as! TicketDetailViewController
        ticketViewController.event_id = Int(event.event_id)
        ticketViewController.modalPresentationStyle = .overCurrentContext
        self.present(ticketViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
